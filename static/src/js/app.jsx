/*!
 * Script for count-up
 * Description: Count-up for my April girl
 * Copyright (C) 2017-present Anh Tranngoc
 * This file is distributed under the same license as the count-up package.
 * Anh Tranngoc <naa@sfc.wide.ad.jp>, 2017.
 */

'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import CountUp from './components/CountUp';

ReactDOM.render(
    <CountUp />,
    document.getElementById('countup')
);
