/**
 * --------------------------------------------------------------------------
 * Aprigi: Dispatcher
 * This file is distributed under the same license as the count-up package.
 * --------------------------------------------------------------------------
 */

'use strict';

import {Dispatcher} from 'flux';

export default new Dispatcher();
